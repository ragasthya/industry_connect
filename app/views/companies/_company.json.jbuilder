json.extract! company, :id, :name, :description, :views_count, :enquires_count, :connects_count, :is_approved, :company_type_id, :created_at, :updated_at
json.url company_url(company, format: :json)
