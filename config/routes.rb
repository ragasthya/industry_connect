Rails.application.routes.draw do
  resources :companies
  resources :company_types
  root 'companies#index'
end
