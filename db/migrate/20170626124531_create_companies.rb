class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text :description
      t.integer :views_count
      t.integer :enquires_count
      t.integer :connects_count
      t.boolean :is_approved
      t.text    :image_data
      t.references :company_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
