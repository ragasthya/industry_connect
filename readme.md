## INDUSTRY CONNECT

### Instructions
Clone the repo. Create your database.yml file and secrets.yml file and run `bundle install` and run `rake db:migrate`

`rake db:drop && rake db:create && rake db:migrate` if you have a older version of this application

### ADMIN:
1.	Adds/Removes/Edits a company and Communication Category.
2.	Cannot add/remove/edit a product of the company.
3.	Communication redressal and forwarding.

### COMPANY:
1.	Adds/Removes/Edits a product.
2.	Addresses to consumer issues via email.
3.	Connects with other companies.
4.	Communication redressal.

### CLASSES:

#### CompanyType: has_many Companies
1.	Name - String				name
2.	Id - Primary key			id

#### Company: has_many Branches, Products
1.	Name - string				name
2.	Description - String			description
3.	Number of times views - int		views_count
4.	Number of times enquired - int	enquires_count
5.	Number of times connected - int	connects_count
6.	Approved - Boolean			is_approved
7.	Logo - String/Image			logo
8.	Company_Type_id - int				theme_id
9.	Number of branches - int		branches_count
10.	Id - Primary key			id

#### Branch:
1.	Address - String			address
2.	Locality - String			locality
3.	City/Town - String			city
4.	State - String				state
5.	Country - String			country
6.	Phone - String				phone
7.	Email - String				email
8.	Company_id - int			company_id
9.	Id - Primary key			id 

#### Product: has_many Sizes
1.	Name - String				name
2.	Description - String			description
3.	Make - String				make
4.	Company id - int			company_id
5.	Id - Primary key			id

#### Size:
1.	Dimensions - String 			dimensions
2.	Weight - String			weight
3.	Size - String				size
4.	Product_id - int			product_id
5.	Id - Primary Key			id

#### Connection:
1.	Company_id - int			company_id
2.	Company_id - int			company_id
3.	Date from - date			date_from
4.	Date to - date				date_to

#### Communication Category: has_many Communications
1.	Name - String				name
2.	Id - Primary key			id

#### Communication:
1.	Subject - String			subject
2.	Text - String				text
3.	Communication_Category_id - int	communication_category_id
4.	Company_id - int from		company_id
5.	Company_id - int to			company_id
6.	Id - Primary Key			id

#### User: has_one Company
1.	Name - String				name
2.	Email - String				email
3.	User Name - String			user_name
4.	Admin - Boolean			is_admin
5.	Company_id - int			company_id
6.	Id - Primary Key			id

### Commands

```ruby
# To Create Company Types
rails generate scaffold CompanyType name:string

# To Create Companies
rails generate scaffold Company name:string description:text views_count:integer enquires_count:integer connects_count:integer is_approved:boolean company_type:references

```

### Versioning
##### 2.1.0
This project uses [SemVer](http://semver.org/)
